package hrefgetter

import (
	"log"

	"golang.org/x/net/html"
)

//ParseHTML takes a html node, and returns a slice of links from that page
func ParseHTML(node *html.Node) []string {
	var links []string
	if node.Type == html.ErrorNode {
		log.Printf("%d is ErrorNode", node.Type)
	} else if node.Type == html.ElementNode && node.Data == "a" {

		for _, a := range node.Attr {
			if a.Key == "href" {
				links = append(links, a.Val)
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		childrenLinks := ParseHTML(c)
		links = append(links, childrenLinks...)
	}
	return links

}
