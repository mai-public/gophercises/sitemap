package parseurl

import (
	"fmt"
	"log"
	"regexp"

	"golang.org/x/net/publicsuffix"
)

// URL contains the Protocol, Domain, and Path
// where Domain is just everything after two forward slashes, but before the path.
type URL struct {
	Protocol string
	Domain   string
	Path     string
}

// ParseURL returns a URL object with the protocol, domain, and path
// where Domain is just everything after two forward slashes, but before the path.
func ParseURL(url string) *URL {
	if string([]rune(url)[0]) == "/" || string([]rune(url)[0]) == "#" {
		return &URL{"", "", url}
	}
	//re, err := regexp.Compile("^(!?(https?)...){,1}+([^/]*?)(/.*)$")
	re, err := regexp.Compile("^(http://|https://|)([^\\!\\*\\(\\)\\;\\@\\&\\=\\+\\$\\,\\/\\?\\%\\#\\[\\]]*)(.*?)$")
	if err != nil {
		log.Fatal(err)
	}
	m := make([]string, 3)
	m = re.FindStringSubmatch(url)
	//log.Println(url)
	//log.Printf("parsed: %s|%s|%s", m[1], m[2], m[3])
	return &URL{m[1], m[2], m[3]}

}

//SameDomain takes a *URL object for both a url and a domain, and returns true if the eTLD+1 are the same for both
func SameDomain(url *URL, domain *URL) (string, bool) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if url.Protocol == "" && url.Domain == "" && url.Path != "" {
		if string([]rune(url.Path)[0]) != "/" {
			log.Printf("URL: '%s' is not same domain", url.Path)
			return "", false
		}

		return fmt.Sprintf("%s%s%s", domain.Protocol, domain.Domain, url.Path), true
	}
	if url.Protocol == "mailto" {
		return "", false
	}
	urlETLD, err := publicsuffix.EffectiveTLDPlusOne(url.Domain)
	if err != nil {
		log.Println(err)
		return "", false
	}
	baseETLD, err := publicsuffix.EffectiveTLDPlusOne(domain.Domain)
	if err != nil {
		log.Println(err)
		return "", false
	}
	if urlETLD == baseETLD {
		return fmt.Sprintf("%s%s%s", url.Protocol, url.Domain, url.Path), true
	}
	return "", false
}
