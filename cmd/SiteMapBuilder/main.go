package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"

	"gitlab.com/mai-public/gophercises/SiteMapBuilder/pkg/hrefgetter"
	"gitlab.com/mai-public/gophercises/SiteMapBuilder/pkg/parseurl"

	"golang.org/x/net/html"
)

var sitemap = make(map[string][]string)

func main() {
	//TODO: Enter homepage flag
	homepage := flag.String("url", "", "Enter homepage to start buidling the sitemap from")
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	//var sitemap needs a mutex to be threadsafe
	var wg sync.WaitGroup
	var m sync.Mutex

	homepageURL := parseurl.ParseURL(*homepage)
	wg.Add(1)
	go worker(&wg, &m, *homepage, *homepageURL)
	wg.Wait()
	fmt.Println(sitemap)
	for k := range sitemap {
		fmt.Println(k)
	}
	// LOOP
	//TODO: Get html of that page

	//TODO: Parse links

	//TODO: Filter URLS so we only have links on the same domain

	//TODO: Add current URL to a list of "parsed urls" with its children being listed as well
	//TODO: Get urls that haven't already been parsed
	//TODO: Restart this loop for those urls that havent been parsed

	// homedir, err := os.UserHomeDir()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fp := homedir + string(os.PathSeparator) + "go_junk" + string(os.PathSeparator) + "ex2.html"
	// root, err := htmlReader(fp)
	// if err != nil {
	// 	log.Fatal(err)
	// }

}

// worker recursive LOOP
// 1. Get html of that page
// 2. Parse links
// 3. Filter URLS so we only have links on the same domain
// 4. Add current URL to a list of "parsed urls" with its children being listed as well
// 5. Get urls that haven't already been parsed
// 6. Restart this loop for those urls that havent been parsed
func worker(wg *sync.WaitGroup, m *sync.Mutex, url string, domain parseurl.URL) {
	defer wg.Done()
	m.Lock()
	if _, ok := sitemap[url]; !ok {
		log.Printf("NEW_PAGE: '%s'", url)
		sitemap[url] = []string{}
		m.Unlock()
	} else {
		m.Unlock()
		return
	}

	resp, err := http.Get(url)
	if err != nil {
		log.Printf("error with url %s: %v", url, err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("error with url %s: %v", url, err)
		return
	}
	root, err := html.Parse(bytes.NewReader(body))
	if err != nil {
		log.Printf("error with url %s: %v", url, err)
		return
	}
	links := hrefgetter.ParseHTML(root)
	var domainLinks []string
	for _, link := range links {
		u := parseurl.ParseURL(link)
		if fullurl, same := parseurl.SameDomain(u, &domain); same {
			if !stringInSlice(fullurl, domainLinks) {
				domainLinks = append(domainLinks, fullurl)
				wg.Add(1)
				go worker(wg, m, fullurl, domain)
				log.Printf("STARTING WORKER: '%s'", fullurl)
			}
		}

	}
	m.Lock()
	sitemap[url] = domainLinks
	m.Unlock()

	return
}

func stringInSlice(str string, sli []string) bool {
	for _, v := range sli {
		if str == v {
			return true
		}
	}
	return false
}

func htmlReader(fp string) (*html.Node, error) {
	bytesHTML, err := ioutil.ReadFile(fp)
	if err != nil {
		return nil, err
	}
	readerHTML := bytes.NewReader(bytesHTML)
	root, err := html.Parse(readerHTML)
	if err != nil {
		return nil, err
	}
	return root, nil
}
